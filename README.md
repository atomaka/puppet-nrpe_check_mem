# puppet-nrpe\_check\_mem

Puppet class to install a Nagios plugin for checking memory.

## Parameters

plugin\_ensure: Controls the check\_mem Perl plugin file (default: present)
puppet\_nrpe: Whether or not you are using the [puppet-module-nrpe](https://github.com/pdxcat/puppet-module-nrpe) module (default: true)
command\_ensure: Manages the command resource provided by puppet-module-nrpe (default: present)
command\_warn: Memory percent for warning level (defaut: 80)
command\_critical: Memory percent for critical level (default: 90)
plugin\_directory: Location the plugin should be installed (default: /usr/lib/nagios/plugins)
