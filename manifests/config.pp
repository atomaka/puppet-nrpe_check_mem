#
class nrpe_check_mem::config {
  $puppet_nrpe      = $nrpe_check_mem::puppet_nrpe
  $command_ensure   = $nrpe_check_mem::command_ensure
  $command_warn     = $nrpe_check_mem::command_warn
  $command_critical = $nrpe_check_mem::command_critical

  if($puppet_nrpe) {
    nrpe::command { 'check_mem':
      ensure  => $command_ensure,
      command => "check_mem -w ${command_warn} -c ${command_critical}",
      require => File['/usr/lib/nagios/plugins/check_mem'],
    }
  }
}
