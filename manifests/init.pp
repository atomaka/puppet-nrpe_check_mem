#
class nrpe_check_mem(
  $plugin_ensure        = $nrpe_check_mem::params::plugin_ensure,
  $puppet_nrpe          = $nrpe_check_mem::params::puppet_nrpe,
  $command_ensure       = $nrpe_check_mem::params::command_ensure,
  $command_warn         = $nrpe_check_mem::params::command_warn,
  $command_critical     = $nrpe_check_mem::params::command_critical,
  $plugin_directory     = $nrpe_check_mem::params::plugin_directory
) inherits nrpe_check_mem::params {
  validate_bool($puppet_nrpe)
  validate_string($plugin_ensure)
  validate_string($command_ensure)
  validate_re($command_warn, ['^\d+$', ''])
  validate_re($command_critical, ['^\d+$', ''])
  validate_absolute_path($plugin_directory)

  include '::nrpe_check_mem::install'
  include '::nrpe_check_mem::config'

  anchor { 'nrpe_check_mem::begin': }
  anchor { 'nrpe_check_mem::end': }

  Anchor ['nrpe_check_mem::begin'] -> Class['::nrpe_check_mem::install']
    -> Class['::nrpe_check_mem::config'] -> Anchor['nrpe_check_mem::end']
}
