#
class nrpe_check_mem::install {
  file { "${nrpe_check_mem::plugin_directory}/check_mem":
    ensure => $nrpe_check_mem::plugin_ensure,
    mode   => '0755',
    source => 'puppet:///modules/nrpe_check_mem/check_mem',
  }
}
