#
class nrpe_check_mem::params {
  $plugin_ensure = present
  $puppet_nrpe = true
  $command_ensure = present
  $command_warn = 80
  $command_critical = 90
  $plugin_directory = '/usr/lib/nagios/plugins'
}
